const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'users',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING
    },
    mail: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    type_user_id : {
      type: Sequelize.INTEGER,
      references: 'type_users' ,// or "conversations"? This is a table name
      referencesKey: 'id' // the PK column name
    }
  },
  {
    timestamps: false
  }
)

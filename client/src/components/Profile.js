import React, { Component } from 'react'
import jwt_decode from 'jwt-decode'
import TicketList from './TicketList'

class Profile extends Component {
  constructor() {
    super()
    this.state = {
      name: '',
      mail: '',
      password: '',
      errors: {}
    }
  }

  componentDidMount() {
    const token = localStorage.usertoken
    const decoded = jwt_decode(token)
    this.setState({
      name: decoded.name,
      mail: decoded.mail,
      id: decoded.id,
      type_user_id: decoded.type_user_id
    })
  }

  render() {
    return (
      
      <div className="container">
        <div className="row">
          <div className="col align-self-end">
            <button type="button" className="btn btn-primary">Solicitar Ticket</button>
          </div>
        </div>
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
            <h1 className="text-center">PERFIL</h1>
          </div>
          <TicketList value={[this.state.id,this.state.type_user_id]}/>

          </div>
      </div>
    )
  }
}

export default Profile

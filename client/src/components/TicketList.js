import React, { Component } from 'react'
import jwt_decode from 'jwt-decode'
import {requestTicket} from './UserFunctions' 
class TicketList extends Component {

    constructor() {
        super()
        this.state = {
          name: '',
          mail: '',
          password: '',
          errors: {}
        }
        
        
       /*  requestTicket(this.props).then(res=>{
           if(res){
               console.log(res)
           }
           else{
               console("no hay nada");
           }
       }) */
      }
    
      componentDidMount() {
        const token = localStorage.usertoken
        const decoded = jwt_decode(token)
        this.setState({
          name: decoded.name,
          mail: decoded.mail,
          id: decoded.id,
          type_user_id: decoded.type_user_id
        })
        this.getTickets();
      }

      getTickets(){
        console.log(this.props)
      }
  render() {
    console.log(this.props)
    return (

        <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">{this.props.value[0]}</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </table>
      
    )
  }
}

export default TicketList
import axios from 'axios';

export const register = newUser => {
  return axios
    .post('users/register', {
      name: newUser.name,
      mail: newUser.mail,
      password: newUser.password
    })
    .then(response => {
      console.log('Registered')
    })
}

export const login = user => {
  return axios
    .post('users/login', {
      mail: user.mail,
      password: user.password
    })
    .then(response => {
      localStorage.setItem('usertoken', response.data)
      return response.data
    })
    .catch(err => {
      console.log(err)
    })
}

export const requestTicket = user=>{
  return axios
  .post('users/ticket',{
    id: user.value[0],
    type_user_id: user.value[1]
  })
  .then(response=>{
    localStorage.setItem('userToken',response.data);
    console.log(response.data);
    return response.data;
  }).catch(err=>{
    console.log(err);
    
  })
}
